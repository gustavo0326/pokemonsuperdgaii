<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class Index_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        //$this->view->debug = true;
        $this->view->render($this,"index","View Title");
    }
    
    public function test(){
        /*$pikachu = new Pokemon(null, "Pikachu", "rata", 4, 2, null, 10, "null", "null");
        $r = $pikachu->create();
        
        $ash = new Trainer(null, "ashk", "some@some.com", "1234", "Ash", 1, 0);
        $r = $ash->create();
        print_r($r);*/
        
        $p = Pokemon::getById(1);
        $t = Trainer::getById(1);
        
        $t->belongsToMany("Pokemons",$p);
        
        print_r($t->update());
        
    }
    
}
